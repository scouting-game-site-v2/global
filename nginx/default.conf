upstream backend-app {
    server server:8080;
}

upstream frontend-app {
    server server:3000;
}

server {
    # listen on port 80 (http)
    server_name _;

    # write access and error logs to /var/log
    access_log /var/log/project_access.log;
    error_log /var/log/project_error.log;

    location / {
        proxy_set_header Host $http_host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_pass http://frontend-app;
    }

    # location /static {
    #     alias /var/www/server/static;
    #     expires 30d;
    # }
    
    location /api {
        proxy_set_header Host $http_host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_pass http://backend-app;
    }

    # websocket
    location /ws {
        proxy_set_header Host $http_host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;

        proxy_http_version 1.1;
        proxy_buffering off;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "Upgrade";
        proxy_pass http://backend-app/ws;
        proxy_read_timeout 36000s;
    }

    # Static files
    location /static {
        alias /usr/share/nginx/html;
        expires 30d;
    }

    # Favicon
    location /favicon.ico {
        alias /usr/share/nginx/html/favicon.ico;
    }
}