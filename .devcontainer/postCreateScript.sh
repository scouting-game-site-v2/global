#!/bin/bash
# Trust the git repositories
git config --global --add safe.directory ${WORKSPACE_FOLDER}
git config --global --add safe.directory ${WORKSPACE_FOLDER}/frontend
git config --global --add safe.directory ${WORKSPACE_FOLDER}/backend
#
# Install the dependencies
cd ${WORKSPACE_FOLDER}/frontend && npm install --force